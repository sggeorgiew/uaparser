﻿using Microsoft.Extensions.Caching.Memory;
using UAParser.Common;

namespace UAParser
{
    /// <summary>
    /// Represents user agent analyzer object
    /// </summary>
    public class UserAgentAnalyzer : IUserAgentAnalyzer
    {
        private readonly IMemoryCache cache;
        private readonly MemoryCacheOptions cacheOptions = new MemoryCacheOptions();

        /// <summary>
        /// User agent settings
        /// </summary>
        public AnalyzerSettings Settings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAgentAnalyzer"/> class
        /// </summary>
        public UserAgentAnalyzer() : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserAgentAnalyzer"/> class
        /// </summary>
        public UserAgentAnalyzer(AnalyzerSettings settings)
        {
            Settings = settings ?? new AnalyzerSettings();
            cacheOptions.SizeLimit = Settings.CacheSizeLimit;
            cache = new MemoryCache(cacheOptions);
        }

        /// <summary>
        /// Parses user agent string to object
        /// </summary>
        /// <param name="userAgent">User agent string</param>
        /// <returns>An <see cref="UserAgent"/> object</returns>
        public UserAgent Parse(string userAgent)
        {
            if (userAgent.Length > Settings.UserAgentSizeLimit)
            {
                userAgent = userAgent.Substring(0, Settings.UserAgentSizeLimit);
            }

            return cache.GetOrCreate(userAgent, entry =>
            {
                entry.SlidingExpiration = Settings.CacheSlidingExpiration;
                entry.Size = 1;

                if (Settings.AbsoluteExpirationRelativeToNow != null)
                {
                    entry.AbsoluteExpirationRelativeToNow = Settings.AbsoluteExpirationRelativeToNow;
                }

                return new UserAgent(userAgent);
            });
        }
    }
}
