﻿namespace UAParser.Models
{
    /// <summary>
    /// Represents browser data
    /// </summary>
    public sealed class Browser
    {
        /// <summary>
        /// Contains the name of browser
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Contains the short version of browser
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Contains the full version of browser
        /// </summary>
        public string FullVersion { get; set; }

        /// <summary>
        /// Contains the engine of browser
        /// </summary>
        public string Engine { get; set; }

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(Version))
            {
                return $"{Name}";
            }

            return $"{Name} {Version}";
        }
    }
}
