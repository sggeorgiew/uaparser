﻿namespace UAParser.Models
{
    /// <summary>
    /// Represents platform types
    /// </summary>
    public static class PlatformType
    {
        /// <summary>
        /// Platform based on ARM architecture
        /// </summary>
        public const string ARM = "ARM";

        /// <summary>
        /// 64-bit platform
        /// </summary>
        public const string X64 = "x64";

        /// <summary>
        /// 32-bit platform
        /// </summary>
        public const string X86 = "x86";

        /// <summary>
        /// Unknown platform type - none value
        /// </summary>
        public const string NONE = "";
    }
}
