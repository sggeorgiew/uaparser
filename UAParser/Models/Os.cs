﻿namespace UAParser.Models
{
    /// <summary>
    /// Represents operating system data
    /// </summary>
    public sealed class Os
    {
        /// <summary>
        /// Contains the name of operating system
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Contains the platform type of operating system (x86, x64)
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
