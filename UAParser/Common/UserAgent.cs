﻿using System;
using UAParser.Models;

namespace UAParser.Common
{
    /// <summary>
    /// Represents user agent object
    /// </summary>
    public class UserAgent
    {
        protected string agent;

        /// <summary>
        /// Indicating whether the user agent is a browser
        /// </summary>
        public bool IsBrowser { get; set; }

        /// <summary>
        /// Indicating whether the user agent is a robot
        /// </summary>
        public bool IsRobot { get; set; }

        /// <summary>
        /// Indicating whether the user agent is a mobile device
        /// </summary>
        public bool IsMobile { get; set; }

        /// <summary>
        /// The OS parsed from the user agent string
        /// </summary>
        public Os OperatingSystem { get; set; }

        /// <summary>
        /// The browser parsed from the user agent string
        /// </summary>
        public Browser Browser { get; set; }

        /// <summary>
        /// Contains the mobile name, if it is a mobile
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Contains the robot name, if it is a robot
        /// </summary>
        public string Robot { get; set; }

        /// <summary>
        /// Creates new user agent object
        /// </summary>
        /// <param name="userAgent">User agent string</param>
        internal UserAgent(string userAgent)
        {
            agent = userAgent.Trim().ToLower();

            ParseOperatingSystem();

            if (ParseRobot()) return;
            if (ParseBrowser()) return;
            if (ParseMobile()) return;
        }

        /// <summary>
        /// Parses the operating system from user agent
        /// </summary>
        /// <returns>True for success, otherwise false</returns>
        internal bool ParseOperatingSystem()
        {
            foreach (var os in Constants.AvailableOperatingSystems)
            {
                if (agent.Contains(os.Key))
                {
                    OperatingSystem = new Os()
                    {
                        Name = os.Value,
                        Platform = ParseOsPlatform()
                    };

                    ParseMobile();

                    return true;
                }
            }

            OperatingSystem = new Os()
            {
                Name = Constants.UNKNOWN_OS,
                Platform = ParseOsPlatform()
            };

            return false;
        }

        /// <summary>
        /// Parses the browser from user agent
        /// </summary>
        /// <returns>True for success, otherwise false</returns>
        internal bool ParseBrowser()
        {
            foreach (var browser in Constants.AvailableBrowsers)
            {
                int browserIndexOf = agent.IndexOf(browser.Key, StringComparison.OrdinalIgnoreCase);
                if (browserIndexOf != -1)
                {
                    string fullVersion = ParseVersion(browserIndexOf, browser.Key);

                    IsBrowser = true;

                    Browser = new Browser()
                    {
                        Name = browser.Value,
                        Version = fullVersion.Split('.')?[0] ?? fullVersion,
                        FullVersion = fullVersion,
                    };

                    return true;
                }
            }

            Browser = new Browser()
            {
                Name = Constants.UNKNOWN_BROWSER,
                Version = string.Empty,
                FullVersion = Constants.NOT_AVAILABLE,
            };

            return false;
        }

        /// <summary>
        /// Parses the robot (if it is) from user agent
        /// </summary>
        /// <returns>True for success, otherwise false</returns>
        internal bool ParseRobot()
        {
            foreach (var robot in Constants.Robots)
            {
                if (agent.Contains(robot.Key))
                {
                    IsRobot = true;
                    Robot = robot.Value;

                    ParseMobile();

                    return true;
                }
            }

            return false;
        }

        internal bool ParseMobile()
        {
            foreach (var mobile in Constants.AvailableMobiles)
            {
                if (agent.Contains(mobile.Key))
                {
                    IsMobile = true;
                    Mobile = mobile.Value;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Parses the operating system platform from user agent
        /// </summary>
        /// <returns></returns>
        private string ParseOsPlatform()
        {
            if (agent.Contains("arm"))
            {
                return PlatformType.ARM;
            }

            if (agent.Contains("wow64") || agent.Contains("x64") || agent.Contains("win64") || agent.Contains("amd64") || agent.Contains("x86_64"))
            {
                return PlatformType.X64;
            }

            return PlatformType.X86;
        }

        /// <summary>
        /// Parses the version from user agent
        /// </summary>
        /// <param name="browserIndexOf">Index of browser from user agent</param>
        /// <returns>Parsed version or empty string</returns>
        private string ParseVersion(int browserIndexOf, string browserName)
        {
            try
            {
                string versionString;

                int msieIndexOf = agent.IndexOf("msie");
                if (msieIndexOf != -1)
                {
                    int versionEndIndexOf = agent.IndexOf(';', msieIndexOf);
                    versionString = agent.Substring(msieIndexOf, versionEndIndexOf - msieIndexOf);

                    var parts = versionString.Split(' ');
                    if (parts?.Length > 1)
                    {
                        return parts[1];
                    }
                }
                else if (browserName.Contains("trident"))
                {
                    int rvIndexOf = agent.IndexOf("rv:");
                    if (rvIndexOf != -1)
                    {
                        int versionEndIndexOf = agent.IndexOf(' ', rvIndexOf);
                        versionString = agent.Substring(rvIndexOf, versionEndIndexOf - rvIndexOf);

                        var parts = versionString.Split(':');
                        if (parts?.Length > 1)
                        {
                            return parts[1];
                        }
                    }
                }
                else
                {
                    int versionStringIndexOf = agent.IndexOf("version");
                    if (versionStringIndexOf != -1)
                    {
                        int versionEndIndexOf = agent.IndexOf(' ', versionStringIndexOf);
                        versionString = agent.Substring(versionStringIndexOf, versionEndIndexOf - versionStringIndexOf);

                        var parts = versionString.Split('/');
                        if (parts?.Length > 1)
                        {
                            return parts[1];
                        }
                    }
                    else
                    {
                        int versionEndIndexOf = agent.IndexOf(' ', browserIndexOf);
                        if (versionEndIndexOf != -1)
                        {
                            versionString = agent.Substring(browserIndexOf, versionEndIndexOf - browserIndexOf);
                        }
                        else
                        {
                            versionString = agent.Substring(browserIndexOf);
                        }

                        var parts = versionString.Split('/');
                        if (parts?.Length > 1)
                        {
                            return parts[1];
                        }
                    }
                }

                return string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
