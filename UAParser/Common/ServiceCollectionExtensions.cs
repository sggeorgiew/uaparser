﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace UAParser.Common
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the user agent analyzer to the Dependency Injection container
        /// </summary>
        /// <param name="services">Dependency Injection container</param>
        /// <returns>Dependency Injection container</returns>
        public static IServiceCollection AddUserAgentService(this IServiceCollection services)
        {
            return services.AddSingleton<IUserAgentAnalyzer, UserAgentAnalyzer>();
        }

        /// <summary>
        /// Adds the user agent analyzer with options to the Dependency Injection container
        /// </summary>
        /// <param name="services">Dependency Injection container</param>
        /// <param name="options">Options for User Agent analyzer</param>
        /// <returns>Dependency Injection container</returns>
        public static IServiceCollection AddUserAgentService(this IServiceCollection services, Action<AnalyzerSettings> options)
        {
            services.AddSingleton<IUserAgentAnalyzer, UserAgentAnalyzer>();
            services.Configure(options);

            return services;
        }
    }
}
