﻿using System.Collections.Generic;

namespace UAParser.Common
{
    /// <summary>
    /// Global contants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Represents unknown browser value
        /// </summary>
        public static readonly string UNKNOWN_BROWSER = "Unknown Browser";

        /// <summary>
        /// Represents unknown OS value
        /// </summary>
        public static readonly string UNKNOWN_OS = "Unknown OS";

        /// <summary>
        /// Represents not available value
        /// </summary>
        public static readonly string NOT_AVAILABLE = "N/A";

        /// <summary>
        /// Known browsers mapped to their internal user agent represent
        /// </summary>
        internal static readonly Dictionary<string, string> AvailableBrowsers = new Dictionary<string, string>
        {
            { "OPR", "Opera" },
            { "Flock", "Flock" },
            { "Brave", "Brave" },
            { "Edge", "Edge" },
            { "Edg", "Edge Chromium" },
            { "SamsungBrowser", "Samsung Browser" },
            { "BaiduBoxApp", "Baidu Box App" },
            { "QtWebEngine", "QtWebEngine Based Browser" },
            { "Chromium", "Chromium" },
            { "UC Browser", "UC Browser" },
            { "UCBrowser", "UC Browser" },
            { "YaBrowser", "Yandex Browser" },
            { "Vivaldi", "Vivaldi" },
            { "Chrome", "Chrome" },
            { "Opera", "Opera" },
            { "Trident" , "Internet Explorer" }, // Trident.* rv
            { "MSIE", "Internet Explorer" },
            { "Internet Explorer", "Internet Explorer" },
            { "Shiira", "Shiira" },
            { "Firefox", "Firefox" },
            { "Chimera", "Chimera" },
            { "Phoenix", "Phoenix" },
            { "Firebird", "Firebird" },
            { "Camino", "Camino" },
            { "Netscape", "Netscape" },
            { "OmniWeb", "OmniWeb" },
            { "Safari", "Safari" },
            { "Minefield", "Minefield" },
            { "qt", "QT based browser" },
            { "S40OviBrowser", "Ovi" },
            { "Mozilla", "Mozilla" },
            { "Konqueror", "Konqueror" },
            { "icab", "iCab" },
            { "Lynx", "Lynx" },
            { "Links", "Links" },
            { "hotjava", "HotJava" },
            { "amaya", "Amaya" },
            { "Dalvik", "Android Browser" },
            { "IBrowse", "IBrowse" },
            { "Maxthon", "Maxthon" },
            { "Ubuntu", "Ubuntu Web Browser" }
        };

        /// <summary>
        /// Known operating systems mapped to their user agent represent
        /// </summary>
        internal static readonly Dictionary<string, string> AvailableOperatingSystems = new Dictionary<string, string>
        {
            { "windows nt 10.0", "Windows 10" },
            { "windows nt 6.3", "Windows 8.1" },
            { "windows nt 6.2", "Windows 8" },
            { "windows nt 6.1", "Windows 7" },
            { "windows nt 6.0", "Windows Vista" },
            { "windows nt 5.2", "Windows 2003" },
            { "windows nt 5.1", "Windows XP" },
            { "windows nt 5.0", "Windows 2000" },
            { "windows nt 4.0", "Windows NT 4.0" },
            { "winnt4.0", "Windows NT 4.0" },
            { "winnt 4.0", "Windows NT" },
            { "winnt", "Windows NT" },
            { "windows 98", "Windows 98" },
            { "win98", "Windows 98" },
            { "windows 95", "Windows 95" },
            { "win95", "Windows 95" },
            { "windows phone", "Windows Phone" },
            { "windows ce", "Windows CE" },
            { "windows", "Unknown Windows OS" },
            { "android", "Android" },
            { "blackberry", "BlackBerry" },
            { "iphone", "iOS" },
            { "ipad", "iOS" },
            { "ipod", "iOS" },
            { "cros", "Chrome OS" },
            { "os x", "Mac OS X" },
            { "ppc mac", "Power PC Mac" },
            { "mac", "Mac OS" },
            { "freebsd", "FreeBSD" },
            { "ppc", "Macintosh" },
            { "debian", "Debian" },
            { "ubuntu", "Ubuntu" },
            { "linux", "Linux" },
            { "sunos", "Sun Solaris" },
            { "apachebench", "ApacheBench" },
            { "hp-ux", "HP-UX" },
            { "netbsd", "NetBSD" },
            { "bsdi", "BSDi" },
            { "openbsd", "OpenBSD" },
            { "amigaos", "Amiga OS" },
            { "gnu", "GNU/Linux" },
            { "x11", "Unix" },
            { "unix", "Unknown Unix OS" },
            { "symbian", "Symbian" },
            { "symbianos", "Symbian OS" },
            { "elaine", "Palm" },
            { "series60", "Symbian S60" }
        };

        internal static readonly Dictionary<string, string> AvailableMobiles = new Dictionary<string, string>()
        {
            // Legacy
            {"mobileexplorer", "Mobile Explorer"},
            {"palmsource", "Palm"},
            {"palmscape", "Palmscape"},
            // Phones and Manufacturers
            {"motorola", "Motorola"},
            {"nokia", "Nokia"},
            {"palm", "Palm"},
            {"iphone", "Apple iPhone"},
            {"ipad", "iPad"},
            {"ipod", "Apple iPod Touch"},
            {"sony", "Sony Ericsson"},
            {"ericsson", "Sony Ericsson"},
            {"blackberry", "BlackBerry"},
            {"cocoon", "O2 Cocoon"},
            {"blazer", "Treo"},
            {"lg", "LG"},
            {"amoi", "Amoi"},
            {"xda", "XDA"},
            {"mda", "MDA"},
            {"vario", "Vario"},
            {"htc", "HTC"},
            {"samsung", "Samsung"},
            {"sharp", "Sharp"},
            {"sie-", "Siemens"},
            {"alcatel", "Alcatel"},
            {"benq", "BenQ"},
            {"ipaq", "HP iPaq"},
            {"mot-", "Motorola"},
            {"playstation portable", "PlayStation Portable"},
            {"playstation 3", "PlayStation 3"},
            {"playstation vita", "PlayStation Vita"},
            {"hiptop", "Danger Hiptop"},
            {"nec-", "NEC"},
            {"panasonic", "Panasonic"},
            {"philips", "Philips"},
            {"sagem", "Sagem"},
            {"sanyo", "Sanyo"},
            {"spv", "SPV"},
            {"zte", "ZTE"},
            {"sendo", "Sendo"},
            {"nintendo dsi", "Nintendo DSi"},
            {"nintendo ds", "Nintendo DS"},
            {"nintendo 3ds", "Nintendo 3DS"},
            {"wii", "Nintendo Wii"},
            {"open web", "Open Web"},
            {"openweb", "OpenWeb"},
            // Operating Systems
            {"android", "Android"},
            {"symbian", "Symbian"},
            {"SymbianOS", "SymbianOS"},
            {"elaine", "Palm"},
            {"series60", "Symbian S60"},
            {"windows ce", "Windows CE"},
            // Browsers
            {"obigo", "Obigo"},
            {"netfront", "Netfront Browser"},
            {"openwave", "Openwave Browser"},
            {"mobilexplorer", "Mobile Explorer"},
            {"operamini", "Opera Mini"},
            {"opera mini", "Opera Mini"},
            {"opera mobi", "Opera Mobile"},
            {"fennec", "Firefox Mobile"},
            // Fallback values
            {"mobile", "Generic Mobile"},
            {"wireless", "Generic Mobile"},
            {"j2me", "Generic Mobile"},
            {"midp", "Generic Mobile"},
            {"cldc", "Generic Mobile"},
            {"up.link", "Generic Mobile"},
            {"up.browser", "Generic Mobile"},
            {"smartphone", "Generic Mobile"},
            {"cellphone", "Generic Mobile"},
        };

        /// <summary>
        /// Known robots mapped to their user agent represent
        /// </summary>
        internal static readonly Dictionary<string, string> Robots = new Dictionary<string, string>()
        {
            { "googlebot", "Googlebot" },
            { "msnbot", "MSNBot" },
            { "baiduspider", "Baiduspider" },
            { "bingbot", "Bing" },
            { "slurp", "Inktomi Slurp" },
            { "yahoo", "Yahoo" },
            { "ask jeeves", "Ask Jeeves" },
            { "fastcrawler", "FastCrawler" },
            { "infoseek", "InfoSeek Robot 1.0" },
            { "lycos", "Lycos" },
            { "yandex", "YandexBot" },
            { "mediapartners-google", "MediaPartners Google" },
            { "CRAZYWEBCRAWLER", "Crazy Webcrawler" },
            { "adsbot-google", "AdsBot Google" },
            { "feedfetcher-google", "Feedfetcher Google" },
            { "curious george", "Curious George" },
            { "ia_archiver", "Alexa Crawler" },
            { "MJ12bot", "Majestic-12" },
            { "Uptimebot", "Uptimebot" }
        };
    }
}
