﻿using UAParser.Common;

namespace UAParser
{
    public interface IUserAgentAnalyzer
    {
        /// <summary>
        /// User agent analyzer settings
        /// </summary>
        AnalyzerSettings Settings { get; set; }

        /// <summary>
        /// Parses user agent string to object
        /// </summary>
        /// <param name="userAgent">User agent string</param>
        /// <returns>An <see cref="UserAgent"/> object</returns>
        UserAgent Parse(string userAgent);
    }
}
