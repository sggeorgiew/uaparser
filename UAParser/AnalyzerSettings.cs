﻿using System;

namespace UAParser
{
    /// <summary>
    /// User agent analyzer settings
    /// </summary>
    public class AnalyzerSettings
    {
        /// <summary>
        /// Maximum size of the cache. The number means it will store that number (not bytes) of different user agents in cache
        /// </summary>
        public int CacheSizeLimit { get; set; } = 5000;

        /// <summary>
        /// Absolute expiration time for a cache entry, relative to now
        /// </summary>
        public TimeSpan? AbsoluteExpirationRelativeToNow { get; set; } = null;

        /// <summary>
        /// How long a cache entry can be inactive (e.g. not accessed) before it will be removed. This will not extend the entry lifetime beyond the absolute expiration (if set)
        /// </summary>
        public TimeSpan CacheSlidingExpiration { get; set; } = TimeSpan.FromDays(3);

        /// <summary>
        /// Maximum size of the user agent string
        /// </summary>
        public int UserAgentSizeLimit { get; set; } = 256;
    }
}
