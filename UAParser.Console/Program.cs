﻿namespace UAParser.Console
{
    using System;
    using System.Collections.Generic;

    class Program
    {
        static void Main()
        {
            List<string> userAgents = new List<string>()
            {
                { "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko" }, // IE 11 on Windows 10
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36 Edg/83.0.478.45" }, // Edge 83 on Windows 10
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0" }, // Firefox 76 on Windows 10
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36" }, // Chrome 83 on Windows 10 (Actually Brave, but the user agent is the same)
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.132 Safari/537.36" }, // Chrome 81 on Windows 10 (Actually Vivaldi, but with chrome user agent)
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 OPR/68.0.3618.125" }, // Opera 68 on Windows 10
                { "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19041" }, // Edge 44 on Windows 10

                { "Mozilla/5.0 (Linux; Android 7.1; vivo 1716 Build/N2G47H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36" }, // Chrome 61 on Android
                { "Mozilla/5.0 (Linux; Android 7.0; SAMSUNG SM-G610M Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/7.4 Chrome/59.0.3071.125 Mobile Safari/537.36" }, // Samsung Browser 7.4 on Android
                { "Dalvik/2.1.0 (Linux; U; Android 7.1.2; AFTA Build/NS6264) CTV" }, // Android Browser on Android
                { "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1" }, // Safari 12.1 on iOS
                { "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)" }, // Internet Explorer 6 on Windows
                { "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)" }, // Internet Explorer 9 on Windows

                { "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a1) Gecko/20070308 Minefield/3.0a1" }, // Minefield 3 on Linux
                { "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:24.0) Gecko/20100101 Firefox/24.0" }, // Firefox 24 on Ubuntu
                { "BrightSign/8.0.69 (XT1143)Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML, like Gecko) QtWebEngine/5.11.2 Chrome/65.0.3325.230 Safari/537.36" }, // QtWebEngine Based Browser 5 on Linux
                { "Opera/9.80 (Linux armv7l) Presto/2.12.407 Version/12.51 , D50u-D1-UHD/V1.5.16-UHD (Vizio, D50u-D1, Wireless)" }, // Opera 12 on Linux
                { "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.34 (KHTML, like Gecko) Qt/4.8.2" }, // QT based browser on Linux
                { "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubuntu/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30" } // Chromium 12 on Linux
            };

            var userAgentAnalyzer = new UserAgentAnalyzer();

            foreach (var userAgent in userAgents)
            {
                var result = userAgentAnalyzer.Parse(userAgent);
                Console.WriteLine($"Browser: {result.Browser} on {result.OperatingSystem}. Is Mobile? {result.IsMobile}");
            }
        }
    }
}
